<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentsTables extends Migration
{
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            
            $table->integer('position')->unsigned()->nullable();

            // feel free to modify the name of this column, but title is supported by default (you would need to specify the name of the column Twill should consider as your "title" column in your module controller if you change it)
            $table->string('title', 200)->nullable();

            // your generated model and form include a description field, to get you started, but feel free to get rid of it if you don't need it
            $table->text('description')->nullable();
            
            $table->unsignedBigInteger('author_id')->nullable();
            $table->foreign('author_id')->references('id')->on('authors');

            $table->unsignedBigInteger('station_id')->nullable();
            $table->foreign('station_id')->references('id')->on('stations');
            
            $table->unsignedBigInteger('category_id')->nullable();
    		$table->foreign('category_id')->nullable()->references('id')->on('categories');
            
            $table->unsignedBigInteger('media_id')->nullable();
            $table->foreign('media_id')->nullable()->references('id')->on('medias');
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            $table->timestamp('publish_start_date')->nullable();
            $table->timestamp('publish_end_date')->nullable();
            $table->timestamp('broadcasting_date')->nullable();
            
            $table->unsignedBigInteger('language_id')->nullable();
            $table->foreign('language_id')->references('id')->on('languages');
            

            $table->unsignedBigInteger('twill_user_id')->nullable();
    		$table->foreign('twill_user_id')->references('id')->on('twill_users');

            $table->text('body')->nullable();
            $table->string('webarea',200)->nullable();
            
			$table->string('media_url',200)->nullable();
			//$table->string('audio_url',200)->nullable();

            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('content_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'content');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('content_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'content');
        });

        Schema::create('content_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'content');
        });
    }

    public function down()
    {
        Schema::dropIfExists('content_revisions');
        Schema::dropIfExists('content_translations');
        Schema::dropIfExists('content_slugs');
        Schema::dropIfExists('contents');
    }
}
