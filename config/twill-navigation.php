<?php

return [
    'contents' => [
        'title' => 'Contenuti',
        'module' => true
    ],
    'categories' => [
        'title' => 'Categorie',
        'module' => true
    ],
    'authors' => [
        'title' => 'Autori/Conduttori',
        'module' => true
    ],
    'stations' => [
        'title' => 'Emittenti',
        'module' => true
    ],
    'trainings' => [
        'title' => 'Percorsi formativi',
        'module' => true
    ],
    'quests' => [
        'title' => 'Suggeriti dalla redazione',
        'module' => true
    ]

];
