<?php

// Register Twill routes here (eg. Route::module('posts'))

Route::module('contents');
Route::module('categories');
Route::module('authors');
Route::module('stations');
Route::module('trainings');
Route::module('quests');