<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Training;

class TrainingRepository extends ModuleRepository
{
    use HandleMedias;

    public function __construct(Training $model)
    {
        $this->model = $model;
    }
}
