<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Quest;

class QuestRepository extends ModuleRepository
{
    use HandleTranslations;

    public function __construct(Quest $model)
    {
        $this->model = $model;
    }
}
