<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Quest;

class QuestTranslation extends Model
{
    protected $baseModuleModel = Quest::class;
}
