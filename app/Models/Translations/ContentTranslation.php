<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\Content;

class ContentTranslation extends Model
{
    protected $baseModuleModel = Content::class;
}
