<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class ContentRevision extends Revision
{
    protected $table = "content_revisions";
}
