<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ContentSlug extends Model
{
    protected $table = "content_slugs";
}
