<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class StationController extends ModuleController
{
    protected $moduleName = 'stations';
}
