<?php

namespace App\Http\Controllers\Admin;
use App\Models\Category;
use App\Models\Author;
use App\Repositories\CategoryRepository;
use App\Repositories\AuthorRepository;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ContentController extends ModuleController
{
    protected $moduleName = 'contents';

   /*
     * Add anything you would like to have available in your module's index view
     */
    protected function indexData($request)
    {
        return [
        	'RubricaList' => app(CategoryRepository::class)->listAll('title', ['title' => 'asc']),
        	'AutoreList' => app(AuthorRepository::class)->listAll('name',$orders = ['name' => 'asc'])
        ];
    }

        /*
     * Add anything you would like to have available in your module's form view
     * For example, relationship lists for multiselect form fields
     */
	protected function formData($request){
		/* preparo liste per le select */
        $cate = app(CategoryRepository::class)->listAll('title',$orders = ['title' => 'asc']);
        $authors = app(AuthorRepository::class)->listAll('name',$orders = ['name' => 'asc']);

        return ['lista_autori' => $authors, 'lista_rubriche' => $cate];
    }

}
