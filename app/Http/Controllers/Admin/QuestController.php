<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class QuestController extends ModuleController
{
    protected $moduleName = 'quests';
}
