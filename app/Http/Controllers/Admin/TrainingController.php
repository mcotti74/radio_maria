<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class TrainingController extends ModuleController
{
    protected $moduleName = 'trainings';
}
