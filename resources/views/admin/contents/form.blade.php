@extends('twill::layouts.form')

@section('contentFields')  

@component('twill::partials.form.utils._columns')


@slot('left')
    @formField('select', [
		'name' => 'author_id',
		'label' => 'Autore',
		'placeholder' => 'Scegli un autore',
		'options' => $lista_autori,
		'searchable' => true
	])
@endslot
@slot('right')
	@formField('select', [
		'name' => 'category_id',
		'label' => 'Rubrica',
		'placeholder' => 'Scegli una rubrica',
		'required' => true,
		'options' => $lista_rubriche,
		'searchable' => true
	])
@endslot
@endcomponent



    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 100
    ])
@stop
